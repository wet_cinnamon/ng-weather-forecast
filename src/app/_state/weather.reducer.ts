import { createFeatureSelector, createSelector } from '@ngrx/store';

import { WeatherActions, WeatherActionTypes } from './weather.actions';
import { initialChartData, initialState } from '../helpers/weather';
import {
  ChartData,
  WeatherForecast,
  WeatherPage
} from '../weather/models/weather.interface';

export function reducer(
  state = initialState,
  action: WeatherActions
): WeatherPage {
  switch (action.type) {
    case WeatherActionTypes.LoadWeather:
      return { ...state, citiesWeatherLoading: true };
    case WeatherActionTypes.LoadWeatherSuccess:
      return {
        ...state,
        citiesWeather: action.weather,
        citiesWeatherLoading: false
      };
    case WeatherActionTypes.LoadWeatherFail:
      return { ...state, citiesWeatherLoading: false };
    case WeatherActionTypes.LoadForecastSuccess:
      return {
        ...state,
        cityForecast: action.forecast,
        cityForecastLoading: false
      };
    case WeatherActionTypes.LoadWeatherFail:
      return { ...state, cityForecastLoading: false };
    case WeatherActionTypes.SelectCity:
      return {
        ...state,
        selectedCityID: action.city,
        cityForecastLoading: true
      };
    default:
      return state;
  }
}

export const weather = createFeatureSelector('weather');
export const citiesWeather = createSelector(
  weather,
  (s: WeatherPage) => s.citiesWeather
);
export const citiesWeatherLoading = createSelector(
  weather,
  (s: WeatherPage) => s.citiesWeatherLoading
);
export const selectedCityID = createSelector(
  weather,
  (s: WeatherPage) => s.selectedCityID
);
export const cityForecast = createSelector(
  weather,
  (s: WeatherPage) => s.cityForecast.slice(0, 8)
); // take only the first eight results
export const cityForecastLoading = createSelector(
  weather,
  (s: WeatherPage) => s.cityForecastLoading
);

export const selectedCityName = createSelector(
  citiesWeather,
  selectedCityID,
  // tslint:disable-next-line:variable-name
  (citiesWeather_, cityID) => {
    const cityWeather = citiesWeather_.find(w => w.id === cityID);
    return (cityWeather && cityWeather.name) || '';
  }
);

// Transorms the forecast data so they can be provided to =>
// the chart @param forecast
const transformToChartData = (forecast: WeatherForecast[]): ChartData[] => {
  return [
    {
      name: '',
      series: forecast.map(f => ({
        value: f.main.temp,
        name: new Date(f.dt * 1000).toLocaleTimeString()
      }))
    }
  ];
};

export const chartData = createSelector(
  cityForecast,
  // tslint:disable-next-line:variable-name
  _forecast => transformToChartData(_forecast) || initialChartData
);

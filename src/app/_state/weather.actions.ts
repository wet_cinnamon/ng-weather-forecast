import { Action } from '@ngrx/store';

import {
  CurrentWeather,
  WeatherForecast
} from '../weather/models/weather.interface';

export enum WeatherActionTypes {
  LoadWeather = '[Weather] Load Weather',
  LoadWeatherSuccess = '[Weather] Load Weather Success',
  LoadWeatherFail = '[Weather] Load Weather Fail',
  LoadForecast = '[Weather] Load Forecast',
  LoadForecastSuccess = '[Weather] Load Forecast Success',
  LoadForecastFail = '[Weather] Load Forecast Fail',
  SelectCity = '[Weather] Select City'
}

export class LoadWeather implements Action {
  readonly type = WeatherActionTypes.LoadWeather;
  constructor(public cities: string[]) {}
}

export class LoadWeatherSuccess implements Action {
  readonly type = WeatherActionTypes.LoadWeatherSuccess;
  constructor(public weather: CurrentWeather[]) {}
}

export class LoadWeatherFail implements Action {
  readonly type = WeatherActionTypes.LoadWeatherFail;
  constructor(public error: Error) {}
}

export class LoadForecast implements Action {
  readonly type = WeatherActionTypes.LoadForecast;
  constructor(public city: number) {}
}

export class LoadForecastSuccess implements Action {
  readonly type = WeatherActionTypes.LoadForecastSuccess;
  constructor(public forecast: WeatherForecast[]) {}
}

export class LoadForecastFail implements Action {
  readonly type = WeatherActionTypes.LoadForecastFail;
  constructor(public error: Error) {}
}

export class SelectCity implements Action {
  readonly type = WeatherActionTypes.SelectCity;
  constructor(public city: number) {}
}

export type WeatherActions =
  | LoadWeather
  | LoadWeatherSuccess
  | LoadWeatherFail
  | SelectCity
  | LoadForecast
  | LoadForecastSuccess
  | LoadForecastFail;

import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { WeatherService } from '../_services/weather.service';
import {
  LoadForecastFail,
  LoadForecastSuccess,
  LoadWeather,
  LoadWeatherFail,
  LoadWeatherSuccess,
  SelectCity,
  WeatherActionTypes
} from './weather.actions';

@Injectable()
export class WeatherEffects {
  // Load the weather data for the cities
  @Effect()
  loadWeather$ = this.actions$.pipe(
    ofType<LoadWeather>(WeatherActionTypes.LoadWeather),
    switchMap(action =>
      this.weatherService.weatherData(action.cities).pipe(
        map(results => new LoadWeatherSuccess(results)),
        catchError(error => of(new LoadWeatherFail(error)))
      )
    )
  );

  // Load the forecast for the first city in the list (after refresh)
  @Effect()
  loadWeatherSuccess$ = this.actions$.pipe(
    ofType<LoadWeatherSuccess>(WeatherActionTypes.LoadWeatherSuccess),
    map(action => new SelectCity(action.weather[0].id))
  );

  // Load forecast for the selected city
  @Effect()
  selectCity$ = this.actions$.pipe(
    ofType<SelectCity>(WeatherActionTypes.SelectCity),

    switchMap(action =>
      this.weatherService.forecast(action.city).pipe(
        map(results => new LoadForecastSuccess(results)),
        catchError(error => of(new LoadForecastFail(error)))
      )
    )
  );

  constructor(
    private actions$: Actions,
    private weatherService: WeatherService
  ) {}
}

import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs';
import {
  CurrentWeather,
  WeatherForecast,
  ChartData,
  WeatherState
} from './models/weather.interface';
import { Store } from '@ngrx/store';
import * as fromWeatherReducer from '../_state/weather.reducer';
import { SelectCity } from '../_state/weather.actions';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WeatherComponent implements OnInit {
  citiesWeather$: Observable<CurrentWeather[]>;
  selectedCityID$: Observable<number>;
  selectedCityForecast$: Observable<WeatherForecast[]>;
  cityName$: Observable<string>;
  chartData$: Observable<ChartData[]>;

  constructor(private store: Store<WeatherState>) {}

  ngOnInit() {
    this.citiesWeather$ = this.store.select(fromWeatherReducer.citiesWeather);
    this.selectedCityID$ = this.store.select(fromWeatherReducer.selectedCityID);
    this.selectedCityForecast$ = this.store.select(
      fromWeatherReducer.cityForecast
    );
    this.cityName$ = this.store.select(fromWeatherReducer.selectedCityName);
    this.chartData$ = this.store.select(fromWeatherReducer.chartData);
  }

  selectCity(id: number) {
    this.store.dispatch(new SelectCity(id));
  }
}

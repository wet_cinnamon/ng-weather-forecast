import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WeatherComponent } from './weather.component';
import { WeatherResolver } from '../_resolvers/weather.resolver';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: WeatherComponent,
    resolve: { WeatherResolver }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WeatherRoutingModule {}

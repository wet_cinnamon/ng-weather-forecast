import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { ChartData, WeatherForecast } from '../models/weather.interface';

@Component({
  selector: 'app-weather-details',
  templateUrl: './weather-details.component.html',
  styleUrls: ['./weather-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WeatherDetailsComponent {
  @Input() data: ChartData[];
  @Input() forecast: WeatherForecast[];
  @Input() cityName: string;

  time(t: number): string {
    return new Date(t * 1000).toLocaleString();
  }
}

export interface WeatherState {
  readonly weather: WeatherPage;
}

export interface WeatherPage {
  citiesWeather: CurrentWeather[];
  citiesWeatherLoading: boolean;
  selectedCityID: number;
  cityForecast: WeatherForecast[];
  cityForecastLoading: boolean;
}

export interface CurrentWeather {
  coord: Coord;
  weather: Weather[];
  base: string;
  main: Main;
  visibility: number;
  wind: Wind;
  clouds: Clouds;
  dt: number;
  sys: Sys;
  id: number;
  name: string;
  cod: number;
}

export interface WeatherForecast {
  weather: Weather[];
  main: Main;
  wind: Wind;
  clouds: Clouds;
  dt: number;
  sys: Sys;
  dt_txt: string;
}

interface Coord {
  lon: number;
  lat: number;
}

interface Weather {
  id: number;
  main: string;
  description: string;
  icon: string;
}

export interface Main {
  temp: number;
  pressure: number;
  humidity: number;
  temp_min: number;
  temp_max: number;
}

export interface ChartData {
  name: string;
  series: {
    value: number;
    name: string;
  }[];
}

interface Wind {
  speed: number;
  deg: number;
  gust: number;
}

interface Clouds {
  all: number;
}

interface Sys {
  type: number;
  id: number;
  message: number;
  country: string;
  sunrise: number;
  sunset: number;
}

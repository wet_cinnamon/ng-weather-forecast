import {
  Component,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import { CurrentWeather } from '../models/weather.interface';

@Component({
  selector: 'app-weather-list',
  templateUrl: './weather-list.component.html',
  styleUrls: ['./weather-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WeatherListComponent {
  @Input() citiesWeather: CurrentWeather[];
  @Input() selectedCityID: number;
  @Output() selectCity = new EventEmitter<number>();
}

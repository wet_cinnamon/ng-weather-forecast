import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WeatherRoutingModule } from './weather-routing.module';
import { WeatherListComponent } from './weather-list/weather-list.component';
import { WeatherDetailsComponent } from './weather-details/weather-details.component';
import { WeatherComponent } from './weather.component';
import { WeatherResolver } from '../_resolvers/weather.resolver';
import { WeatherService } from '../_services/weather.service';
import { HttpClientModule } from '@angular/common/http';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { StoreModule } from '@ngrx/store';
import * as fromWeather from '../_state/weather.reducer';
import { EffectsModule } from '@ngrx/effects';
import { WeatherEffects } from '../_state/weather.effects';

@NgModule({
  declarations: [
    WeatherListComponent,
    WeatherDetailsComponent,
    WeatherComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    WeatherRoutingModule,
    NgxChartsModule,
    StoreModule.forFeature('weather', fromWeather.reducer),
    EffectsModule.forFeature([WeatherEffects])
  ],
  exports: [WeatherComponent],
  providers: [WeatherResolver, WeatherService]
})
export class WeatherModule {}

import { WeatherPage, ChartData } from '../weather/models/weather.interface';

export const initialState: WeatherPage = {
  citiesWeather: [],
  citiesWeatherLoading: false,
  selectedCityID: 0,
  cityForecast: [],
  cityForecastLoading: false
};

export const initialChartData: ChartData[] = [
  {
    name: '',
    series: [{ value: 0, name: '' }]
  }
];

import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot
} from '@angular/router';
import { Store } from '@ngrx/store';
import { LoadWeather } from '../_state/weather.actions';

@Injectable()
export class WeatherResolver implements Resolve<any> {
  constructor(private store: Store<any>) {}

  resolve(): boolean {
    // cities weather data
    this.store.dispatch(
      new LoadWeather([
        '2759794', // Amsterdam
        '2643123', // Manchester
        '2650225', // Edinburgh
        '1864134', // Fuji
        '5879092', // Alaska
        '2192628' // Canterbury
      ])
    );
    return true;
  }
}

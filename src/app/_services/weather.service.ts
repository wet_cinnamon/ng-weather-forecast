import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {
  CurrentWeather,
  WeatherForecast
} from '../weather/models/weather.interface';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  constructor(private http: HttpClient) {}

  weatherData(cities: string[]): Observable<CurrentWeather[]> {
    return this.http
      .get<any>(
        environment.api_url +
          'group?id=' +
          cities.join(',') +
          '&appid=' +
          environment.api_key +
          '&units=metric'
      )
      .pipe(map(response => response.list));
  }

  forecast(city: number): Observable<WeatherForecast[]> {
    return this.http
      .get<any>(
        environment.api_url +
          'forecast?id=' +
          city +
          '&appid=' +
          environment.api_key +
          '&units=metric'
      )
      .pipe(map(response => response.list));
  }
}

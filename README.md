# ng-weather-forecast

weather-forecast application, related for Backbase assesment.

# Development

- Angular CLI v.8.0.3
- RxJS v.6.4.0
- NgRX v.8.0.1
- ngx-charts v.12.0.1

## Development server

Clone the project, run `npm install` and then `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Architecture

The [app.component] which specifies the layout of the page. The page consists of a [navbar] component & a [router-outlet]. The [weather.module] is a lazy loaded module, so it is loaded when we navigate to this route. In our case, because it's the only route that we have in the application and because we redirect to this path ([app-routing.module.ts] - path: ['weather-forecast']) when we navigate to the initial route, lazy-loading does not offer something in the performance but when we design an application, we should have on our minds that will be added more modules and routes in the future.

## Weather module

The [weather.module] consists of three components ([weather.component], [weather-list.component], [weather-details.component]). The [weather.component] is a smart component which "knows" how to retrieve the weather data from the api and provides with these data the other two dumb components.

## shared module

We used it as a 'server' of dependencies to multiple modules in our app, especially for graphical libraries.

## Design pattern

For this app, we followed the Redux design pattern. We implemented it here through the ngrx/platform library. On each action, for example, when the user selects a new city from the list, an action is dispatched. Based on the payload of this action, the reducer (weather.reducer) provides the updated state of the app. If an action has a side effect then this side effect is handled by the corresponding effect (weather.effects). The smart component (weather.component) observes the changes in the state of our app through specific selectors (you can find these selectors at the end of weather.reducer.ts file) and provides the dumb components with specific '@Inputs()'. The dumb components depend only on their inputs to render the appropriate information. This dependance of the dumpb components only on their inputs and the fact that these inputs change in an immutable way, is significant, because we can perform changeDetectionStrategy.OnPush in our components. This strategy has performance benefits for our app.

## Unit testing

I had no time to complete the unit tests(i prefer Jest).
